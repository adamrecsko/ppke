package com.epam.testability;

public class Validator {
    public Validator() {
    }

    public void validateNotNull(final Object input) {
        if (input == null) {
            throw new IllegalArgumentException("Null parameter is illegal!");
        }
    }
}
