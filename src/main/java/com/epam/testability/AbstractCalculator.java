package com.epam.testability;

public abstract class AbstractCalculator implements Calculator {
    private void validateInput(final Integer a, final Integer b) {
        ValidatorProvider.getValidator().validateNotNull(a);
        ValidatorProvider.getValidator().validateNotNull(b);
    }
    public final Integer calculate(final Integer a, final Integer b) {
        validateInput(a, b);
        return doCalculate(a, b);
    }

    protected abstract Integer doCalculate(Integer a, Integer b);
}
