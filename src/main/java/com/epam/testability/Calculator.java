package com.epam.testability;

public interface Calculator {
    public Integer calculate(Integer a, Integer b);
}
