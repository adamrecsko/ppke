package com.epam.testability;

public class ValidatorProvider {
    
	 private static Validator mockValidator;
	
	 public static Validator getValidator(){
		 Validator validator ;
		 if (mockValidator!=null){
			 validator = mockValidator;
		 } else{
			 validator = new Validator();
		 }
		 return validator;
	 }
	 
	 public static void setMockValidator(Validator validator){
		 mockValidator = validator;
	 }
	 
	
	
}
