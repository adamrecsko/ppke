package com.epam.testability;

public class NumberFormatter {
    public String format(Integer number) {
        return "[" + number + "]";
    }
}
