package com.epam.testability;

public class Sum extends AbstractCalculator {

    @Override
    protected Integer doCalculate(final Integer a, final Integer b) {
        return a + b;
    }
}
