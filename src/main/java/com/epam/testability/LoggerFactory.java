package com.epam.testability;

import java.util.logging.Logger;

public class LoggerFactory {
    private static Logger mockLogger;

    public static void setMockLogger(final Logger mockLogger) {
        LoggerFactory.mockLogger = mockLogger;
    }

    public static <T> Logger getLogger(final Class<T> clazz) {
        Logger result;
        if (mockLogger != null) {
            result = mockLogger;
        } else {
            result = Logger.getLogger(clazz.getName());
        }
        return result;
    }
}
