package ppke.testaut.mocking;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.junit.Before;
import org.junit.Test;

import com.epam.testability.AbstractCalculator;
import com.epam.testability.LoggerFactory;
import com.epam.testability.MathProvider;
import com.epam.testability.NumberFormatter;
import com.epam.testability.Sum;
import com.epam.testability.Validator;
import com.epam.testability.ValidatorProvider;

import org.junit.Assert;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

/**
 * Unit test for simple App.
 */
public class AppTest {

	@Mock
	private Logger logger;
	
	@Mock
	private AbstractCalculator calculator;	

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		LoggerFactory.setMockLogger(logger);
	}

	@Test
	public void testSumDoCalculateShouldReturnSumOf2Number() {
		// GIVEN
		Integer a = new Integer(10);
		Integer b = new Integer(20);
		Sum sum = new Sum();
		// WHEN
		Integer result = sum.calculate(a, b);
		// THEN
		Assert.assertEquals(result, new Integer(30));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSumDoCalculateShouldThrowsExceptionIfANull() {
		// GIVEN
		Integer a = null;
		Integer b = new Integer(10);
		Sum sum = new Sum();
		sum.calculate(a, b);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSumDoCalculateShouldThrowsExceptionIfBNull() {
		// GIVEN
		Integer a = new Integer(10);
		Integer b = null;
		Sum sum = new Sum();
		sum.calculate(a, b);
	}
	
	@Test
	public void testSumDoCalculateShouldUseValidator() {
		// GIVEN
		Integer a = new Integer(10);
		Integer b = new Integer(12);
		Validator validator = BDDMockito.mock(Validator.class);
		ValidatorProvider.setMockValidator(validator);
		Sum sum = new Sum();
		sum.calculate(a, b);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testValidatorShouldThrowException() {
		Validator validator = new Validator();
		validator.validateNotNull(null);
	}

	@Test
	public void testNumberFormatterShouldReturnWellFormat() {
		// GIVEN
		NumberFormatter formatter = new NumberFormatter();
		// WHEN
		String result = formatter.format(new Integer(10));
		// THEN
		Assert.assertEquals(result, "[10]");
	}

	@Test
	public void testLoggerFactoryShouldReturnTheMockLogger() {
		// GIVEN
		Logger _logger = LoggerFactory.getLogger(this.getClass());
		// WHEN
		Assert.assertEquals(_logger, this.logger);
	}
	@Test
	public void testMathProviderSumShouldReturnTheSumValueOfList(){
		//GIVEN
		MathProvider mp = new MathProvider();
		mp.setCalc(this.calculator);
		List<Integer> list = new ArrayList<Integer>();
		list.add(new Integer(10));
		list.add(new Integer(10));
		BDDMockito.given(calculator.calculate(Mockito.anyInt(),Mockito.anyInt())).willReturn(20);
		//WHEN
	    String result = mp.sum(list);
	    //THEN
		Assert.assertEquals(result, "[20]");
	}

}
